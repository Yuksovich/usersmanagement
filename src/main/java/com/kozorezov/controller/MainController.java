package com.kozorezov.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
    private static final String INDEX_PAGE = "index";
    @RequestMapping(
            path = "/",
            method = RequestMethod.GET)
    public String getIndex() {
        return INDEX_PAGE;
    }
}
