package com.kozorezov.controller;

import java.util.List;

public class ControllerResult {
    private Status status;
    private List errorMessageList;

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public List getErrorMessageList() {
        return errorMessageList;
    }

    public void setErrorMessageList(final List errorMessageList) {
        this.errorMessageList = errorMessageList;
    }

    public enum Status {
        SUCCESS, FAIL
    }

}
