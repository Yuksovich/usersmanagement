package com.kozorezov.controller;

import com.kozorezov.dto.PasswordDto;
import com.kozorezov.exception.UserNotFoundException;
import com.kozorezov.exception.UsersException;
import com.kozorezov.model.User;
import com.kozorezov.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/user")
public class UserRestController {
    private final UserService userService;
    private final MessageSource messageSource;

    @Autowired
    public UserRestController(final UserService userService, final MessageSource messageSource) {
        this.userService = userService;
        this.messageSource = messageSource;
    }

    @RequestMapping(method = RequestMethod.GET, produces = {"application/json; charset=UTF-8"})
    public List<User> getAllUsers() {
        return userService.retrieveAll();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/{id}", produces = {"application/json; charset=UTF-8"})
    public User getUser(@PathVariable("id") final int id) {
        final User user = userService.retrieve(id);
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }

    @RequestMapping(method = RequestMethod.POST, produces = {"application/json; charset=UTF-8"})
    public ControllerResult saveUser(@ModelAttribute("user") @Valid final User user, final BindingResult result) {
        final ControllerResult savingResult = new ControllerResult();
        if (!result.hasErrors()) {
            userService.createOrUpdate(user);
            savingResult.setStatus(ControllerResult.Status.SUCCESS);
        } else {
            savingResult.setStatus(ControllerResult.Status.FAIL);
            savingResult.setErrorMessageList(result.getFieldErrors());
        }
        return savingResult;
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
    public void delete(@PathVariable("id") final int id) {
        userService.delete(id);
    }

    @RequestMapping(path = "/change_password",
            method = RequestMethod.POST,
            produces = {"application/json; charset=UTF-8"})
    public ControllerResult changePassword(
            @ModelAttribute("passwordDto") @Valid final PasswordDto passwordDto, final BindingResult result) {

        final ControllerResult changingResult = new ControllerResult();
        if (!result.hasErrors()) {
            userService.changePassword(passwordDto);
            changingResult.setStatus(ControllerResult.Status.SUCCESS);
        } else {
            changingResult.setStatus(ControllerResult.Status.FAIL);
            changingResult.setErrorMessageList(result.getFieldErrors());
        }
        return changingResult;
    }

    @RequestMapping(path = "/reset_password",
            method = RequestMethod.POST,
            produces = {"application/json; charset=UTF-8"})
    public ControllerResult resetPassword(
            @ModelAttribute("passwordDto") @Valid final PasswordDto passwordDto, final BindingResult result) {
        final ControllerResult resetResult = new ControllerResult();
        if (!result.hasErrors()) {
            userService.resetPassword(passwordDto);
            resetResult.setStatus(ControllerResult.Status.SUCCESS);
        } else {
            resetResult.setStatus(ControllerResult.Status.FAIL);
            resetResult.setErrorMessageList(result.getFieldErrors());
        }
        return resetResult;
    }

    @ExceptionHandler(UsersException.class)
    public ControllerResult handleException(final UsersException ex) {
        final ControllerResult result = new ControllerResult();
        final FieldError fieldError = new FieldError(
                ex.toString(),
                ex.getField(),
                messageSource.getMessage(ex.getMessageSource(), null, Locale.getDefault())
        );
        result.setStatus(ControllerResult.Status.FAIL);
        result.setErrorMessageList(Collections.singletonList(fieldError));
        return result;
    }
}
