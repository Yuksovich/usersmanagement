package com.kozorezov.dto;

import org.hibernate.validator.constraints.Length;

public class PasswordDto {
    private int id;
    private String currentPassword;
    @Length(min = 3, message = "{password.toShort}")
    private String newPassword;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(final String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }
}
