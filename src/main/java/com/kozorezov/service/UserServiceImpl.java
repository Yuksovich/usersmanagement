package com.kozorezov.service;

import com.kozorezov.dto.PasswordDto;
import com.kozorezov.exception.IncorrectPasswordException;
import com.kozorezov.exception.UserNotFoundException;
import com.kozorezov.model.User;
import com.kozorezov.repository.AppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final AppRepository<User> repository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(final AppRepository<User> repository, final PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createOrUpdate(final User user) {
        final Integer id = user.getId();
        if (id != null) {
            final User userToUpdate = retrieve(id);
            if (userToUpdate == null) {
                throw new UserNotFoundException();
            }
            if (!passwordEncoder.matches(user.getPassword(), userToUpdate.getPassword())) {
                throw new IncorrectPasswordException("password", "user.incorrectPassword");
            }
        }
        encodePasswordAndSaveUser(user.getPassword(), user);
    }

    @Override
    public List<User> retrieveAll() {
        return repository.findAll();
    }

    @Override
    public User retrieve(final int id) {
        return repository.find(id);
    }

    @Override
    public void delete(final int id) {
        repository.delete(id);
    }

    @Override
    public void changePassword(final PasswordDto passwordDto) {
        final User user = repository.find(passwordDto.getId());
        if (user == null) {
            throw new UserNotFoundException();
        }
        if (!passwordEncoder.matches(passwordDto.getCurrentPassword(), user.getPassword())) {
            throw new IncorrectPasswordException("currentPassword", "user.incorrectPassword");
        }
        encodePasswordAndSaveUser(passwordDto.getNewPassword(), user);
    }

    @Override
    public void resetPassword(final PasswordDto passwordDto) {
        final User user = repository.find(passwordDto.getId());
        if (user == null) {
            throw new UserNotFoundException();
        }
        encodePasswordAndSaveUser(passwordDto.getNewPassword(), user);
    }

    private void encodePasswordAndSaveUser(final String password, final User user) {
        final String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);
        repository.saveOrUpdate(user);
    }
}
