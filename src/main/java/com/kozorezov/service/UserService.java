package com.kozorezov.service;

import com.kozorezov.dto.PasswordDto;
import com.kozorezov.model.User;

import java.util.List;

public interface UserService {
    void createOrUpdate(User entity);

    User retrieve(int id);

    List<User> retrieveAll();

    void delete(int id);

    void changePassword(PasswordDto passwordDto);

    void resetPassword(PasswordDto passwordDto);
}
