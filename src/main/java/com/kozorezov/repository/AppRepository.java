package com.kozorezov.repository;

import java.util.List;

public interface AppRepository<Entity> {
    List<Entity> findAll();

    Entity find(int id);

    void saveOrUpdate(Entity entity);

    void delete(int id);
}
