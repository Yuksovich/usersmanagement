package com.kozorezov.repository;

import com.kozorezov.exception.UniquenessViolationException;
import com.kozorezov.exception.UserNotFoundException;
import com.kozorezov.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository implements AppRepository<User> {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepository(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
    }

    @Override
    public User find(final int id) {
        final Session session = sessionFactory.getCurrentSession();
        try {
            return session.get(User.class, id);
        } finally {
            session.clear();
        }
    }

    @Override
    public void saveOrUpdate(final User user) {
        final Session session = sessionFactory.getCurrentSession();
        final Criteria criteria = session.createCriteria(User.class);
        criteria.add(
                Restrictions.and(
                        Restrictions.neOrIsNotNull("id", user.getId()),
                        Restrictions.eq("login", user.getLogin())));
        if (criteria.list().size() > 0) {
            throw new UniquenessViolationException("login", "user.loginExists");
        }
        session.saveOrUpdate(user);
    }

    @Override
    public void delete(final int id) {
        final User userToDelete = find(id);
        if (userToDelete == null) {
            throw new UserNotFoundException();
        }
        sessionFactory.getCurrentSession().delete(userToDelete);
    }
}
