package com.kozorezov.exception;

public class IncorrectPasswordException extends UsersException {
    public IncorrectPasswordException(final String field, final String messageSource) {
        super(field, messageSource);
    }
}
