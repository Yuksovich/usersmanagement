package com.kozorezov.exception;

public class UniquenessViolationException extends UsersException {
    public UniquenessViolationException(final String field, final String messageSource) {
        super(field, messageSource);
    }
}
