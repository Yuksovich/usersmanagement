package com.kozorezov.exception;

public class UsersException extends RuntimeException {
    private final String field;
    private final String messageSource;

    public UsersException(final String field, final String messageSource) {
        super();
        this.field = field;
        this.messageSource = messageSource;
    }

    public String getField() {
        return field;
    }

    public String getMessageSource() {
        return messageSource;
    }
}
