package com.kozorezov.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "user.notFound")
public class UserNotFoundException extends RuntimeException {
}
