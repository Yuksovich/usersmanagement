package com.kozorezov.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Table(name = "USER_TABLE")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "FIRST_NAME", length = 50)
    @Pattern(regexp = "[a-zA-Zа-яА-Я]*", message = "{name.pattern}")
    @Length(max = 50, message = "{name.toLong}")
    private String firstName;

    @Column(name = "LAST_NAME", length = 50)
    @Pattern(regexp = "[a-zA-Zа-яА-Я]*", message = "{name.pattern}")
    @Length(max = 50, message = "{name.toLong}")
    private String lastName;

    @Column(name = "BIRTH_DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Past(message = "{date.notInPast}")
    private Date birthDate;

    @Column(name = "LOGIN", length = 50, unique = true)
    @NotEmpty(message = "{login.empty}")
    @Length(max = 50, message = "{login.toLong}")
    private String login;

    @Column(name = "PASSWORD", length = 60, nullable = false)
    @NotEmpty(message = "{password.empty}")
    @Length(min = 3, message = "{password.toShort}")
    private String password;

    @Column(name = "ABOUT", length = 500)
    @Length(max = 500, message = "{about.toLong}")
    private String about;

    @Column(name = "ADDRESS", length = 200)
    @Length(max = 200, message = "{address.toLong}")
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(final Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(final String about) {
        this.about = about;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }
}
