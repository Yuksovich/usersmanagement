$(document).ready(function () {
    var table = $('#users-table');
    var dataTable = table.DataTable({
        'ajax': '/api/user',
        'sAjaxDataProp': '',
        'columns': [
            {
                data: 'id',
                visible: false
            },
            {data: 'firstName'},
            {data: 'lastName'},
            {data: 'birthDate'},
            {
                data: 'login',
                render: function (data, type, full) {
                    return '<a href="/" user=' + full + '>' + data + '</a>';
                }
            },
        ],
        'createdRow': function (row) {
            $(row).addClass('table-row');
        }
    });

    var tableBody = table.find('tbody');
    tableBody.find('tr').on('click', function () {
        var tr = $(this).find('tr');
        var row = dataTable.row(tr);
        var user = row.data();
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child(showDetails(user)).show();
        }
    });

    var desc;
    table.on('click', '.table-row', function (event) {
        event.preventDefault();
        var row = dataTable.row($(this));
        var user = row.data();
        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            if(desc){
                desc.remove();
            }
            desc = row.child(showDetails(user));
            desc.show();
            $('.edit').click(function () {
                showUserDialog(user);
            });
            $('.delete').click(function () {
                showDeleteUserDialog(user.id);
            });
            $('.changePassword').click(function () {
                showChangePasswordDialog(user, false, 'api/user/change_password');
            });
            $('.resetPassword').click(function () {
                showChangePasswordDialog(user, true, 'api/user/reset_password');
            });
        }
    });

    $('#new-user-btn').on('click', function () {
        showUserDialog(null);
    });
});

function showDetails(user) {
    return '<div>' +
        '<div><h3><b>Login: </b>' + user.login + '</h3></div>' +
        '<div><h3><b>Full name: </b>' + user.firstName + " " + user.lastName + '</h3>' +
        '<h4><b>Birth date: </b>' + user.birthDate + '</h4></div>' +
        '<div class="about"><b>About:</b><br>' + user.about + '</div>' +
        '<div><b>Address:</b><br>' + user.address + '</div>' +
        '<hr/>' +
        '<div ><button class="btn-info edit">Edit</button>' +
        '<button class="btn-success changePassword">Change password</button>' +
        '<button class="btn-default resetPassword">Reset password</button>' +
        '<button class="btn-danger delete">Delete</button></div>' +
        '</div>';
}
