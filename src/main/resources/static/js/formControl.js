function showUserDialog(user) {
    $.get('userForm.html', function (form) {
        var dialog = bootbox.dialog({
            title: 'User form',
            message: form,
            closeButton: true
        });

        var userForm = dialog.find('form');
        var message = 'New user added';
        if (user) {
            message = 'User changed successfully';
            $('.confirmPasswordControlGroup').remove();
            for (var field in user) {
                $('.edit-user-form').find('[name=' + field + ']').val(user[field]);
            }
        }

        validateForm();

        userForm.submit(function (event) {
            event.preventDefault();
            onFormSubmitted(userForm, '.edit-user-form', 'api/user', function () {
                bootbox.alert(message, function () {
                    window.location = "/";
                });
            });
        });
    }, 'html');
}

function showChangePasswordDialog(user, isReset, address) {
    $.get('passwordForm.html', function (form) {
        bootbox.dialog({
            title: 'Change password',
            message: form,
            closeButton: true
        });

        validateForm();

        if (isReset) {
            $('.currentPasswordControlGroup').remove();
        }

        var changePasswordForm = $('.password-user-form');
        changePasswordForm.find('[name=id]').val(user.id);
        changePasswordForm.submit(function (event) {
            event.preventDefault();
            onFormSubmitted(changePasswordForm, '.password-user-form', address, function () {
                bootbox.alert('Password changed successfully', function () {
                    window.location = '/';
                });
            });
        });
    }, 'html');
}

function onFormSubmitted(form, formClass, requestAddress, successCallback) {
    $('.has-error').html('');
    $('.form-group').removeClass('error');
    $.ajax({
        type: 'POST',
        url: requestAddress,
        data: form.serialize(),
        success: function (response) {
            if (response.status === 'FAIL') {
                showErrors(response, formClass);
            } else {
                successCallback();
            }
        }
    });
}

function showErrors(response, formClass) {
    for (var i = 0; i < response.errorMessageList.length; i++) {
        var item = response.errorMessageList[i];
        var controlGroup = $(formClass).find('.' + item.field + 'ControlGroup');
        controlGroup.addClass('error');
        controlGroup.find('.has-error').html(item.defaultMessage);
    }
}

function showDeleteUserDialog(userId) {
    bootbox.confirm({
        message: 'Are you sure you want to delete this entry?',
        buttons: {
            confirm: {
                label: 'Delete',
                className: 'btn-danger'
            },
            cancel: {
                label: 'Cancel',
                className: 'btn-default'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: '/api/user/' + userId,
                    method: 'DELETE',
                    success: function () {
                        window.location = '/';
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        bootbox.alert(jqXHR.responseJSON.message);
                        window.location = '/';
                    }
                });
            }
        }
    });
}

function validateForm() {
    $.validate({
        form: '.form',
        modules: 'security'
    });
}